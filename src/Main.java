// Deivid de Paula Mafra
// ID: 752173
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		double base = 0;
		double height = 0;
		double side = 0;
		String color = "";

		ArrayList<Shape> shapeList = new ArrayList<Shape>();
		while (choice !=3) {
			// 1. show the menu
			showMenu();
			

			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();

			// 3. actions
			switch (choice) {
			case 1:

				System.out.println("1 You entered:  " + choice);

				System.out.println("Enter the Triangle base: ");
				base = keyboard.nextDouble();
				System.out.println("Enter the Triangle height: ");
				height = keyboard.nextDouble();
				System.out.println("Enter the Triangle color: ");
				color = keyboard.next();

				Triangle triangle = new Triangle(color, base, height);
				triangle.printInfo();
				shapeList.add(triangle);
				break;

			case 2:

				System.out.println("2 You entered: " + choice);

				System.out.println("Enter the Square side: ");
				side = keyboard.nextDouble();
				System.out.println("Enter the Square color: ");
				color = keyboard.next();

				Square square = new Square(color, side);
				square.printInfo();
				shapeList.add(square);
				break;
			default:

			}
		
		}
		for (Shape shape : shapeList) {
			shape.printInfo();
		}

	}

	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
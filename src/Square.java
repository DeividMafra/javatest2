
public class Square extends Shape implements TwoDimensionalShapeInterface{

	private double side;

	public void setSide(double side) {
		this.side = side;
	}

	public Square(String color, double side) {
		super(color);
		this.side = side;
	}

	public double getSide() {
		return side;
	}

	@Override
	public double calculateArea() {

		return side*side;
	}
	@Override
	public void printInfo() {
		super.printInfo();
		System.out.println("The dimensions of Square is: "+ getSide()+"\n" 
				+ "for sides of: "+side);
		System.out.println("The area of Square is: "+ calculateArea());
		
	}
}

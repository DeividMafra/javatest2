public class Triangle extends Shape implements TwoDimensionalShapeInterface {
	private double base;
	private double height;
	
	public Triangle(String color, double base, double height) {
		super(color);
		this.base = base;
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double calculateArea() {
		
		return .5*base*height;
	}
	
	@Override
	public void printInfo() {
		super.printInfo();
		System.out.println("The dimensions of Triangle is:\n"
				+ "base: "+base+"\nheight: "+height);
		System.out.println("The area of Triangle is: "+ calculateArea());
	}
}

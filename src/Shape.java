
public class Shape implements TwoDimensionalShapeInterface{
	private String color;
	
	public void setColor(String color) {
		this.color = color;
	}

	public Shape(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	@Override
	public double calculateArea() {
	
		return 0;
	}

	@Override
	public void printInfo() {
		System.out.println("The color of format is: "+ getColor());
		
	}
	
	

}
